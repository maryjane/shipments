import enum


class StatusCategory(enum.Enum):
    LABEL_CREATED = 0
    IN_TRANSIT = 1
    DELIVERED = 2


class Address:
    def __init__(self):
        self.name = None
        self.attention = None
        self.address1 = None
        self.address2 = None
        self.address3 = None
        self.city = None
        self.state = None
        self.postalcode = None
        self.country = None

    def __str__(self):
        res = ''
        if self.name and self.name != '':
            res += self.name + '\n'
        if self.attention and self.attention != '':
            res += self.attention + '\n'
        if self.address1 and self.address1 != '':
            res += self.address1 + '\n'
        if self.address2 and self.address2 != '':
            res += self.address2 + '\n'
        if self.address3 and self.address3 != '':
            res += self.address3 + '\n'
        if self.postalcode and self.postalcode != '':
            res += self.postalcode + ' '
        if self.city and self.city != '':
            res += self.city + '\n'
        if self.state and self.state != '':
            res += self.state + '\n'
        if self.country and self.country != '':
            res += self.country
        return res


class PackageInfo:
    def __init__(self, code, carrier):
        self.code = code
        self.carrier = carrier
        self.carrier_name = None

        self.status = None
        self.status_category = None
        self.events = []

        # Note from the delivering company about location
        self.delivery_note = None

        # Description of package contents from sender
        self.description = None

        self.destination = None
        self.origin = None

        self.weight = None
        self.service = None


class PackageEvent:
    def __init__(self, timestamp, location, label):
        self.timestamp = timestamp
        self.location = location
        self.label = label

    def __repr__(self):
        return f'<event {self.timestamp} {self.label} @ {self.location}>'
