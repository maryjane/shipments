from datetime import datetime

import requests

from shipments.structs import PackageInfo, PackageEvent, StatusCategory, Address


class Carrier:
    DISPLAYNAME = 'N/A'

    @classmethod
    def identify(cls, code):
        pass

    @classmethod
    def get_requirements(cls, code):
        return []

    def get_info(self, code, extra):
        pass


class PostNL(Carrier):
    DISPLAYNAME = 'PostNL'

    @classmethod
    def identify(cls, code):
        if code.startswith('3S'):
            if len(code) == 13:
                return True
        if code.startswith('KG'):
            if len(code) == 8:
                return True
        return False

    @classmethod
    def get_requirements(cls, code):
        return [
            ('country', 'Destination country', 'country-alpha2:NL'),
            ('postalcode', 'Postal code', 'text')
        ]

    def get_info(self, code, extra):
        if not 'country' in extra:
            raise ValueError("Missing 'country' in extra data")
        if not 'postalcode' in extra:
            raise ValueError("Missing 'postalcode' in extra data")
        country = extra['country'].upper()
        postalcode = extra['postalcode'].upper().replace(' ', '').replace('-', '')
        url = f'https://jouw.postnl.nl/track-and-trace/api/trackAndTrace/{code}-{country}-{postalcode}?language=en'
        response = requests.get(url)
        data = response.json()

        data = data['colli'][code]

        result = PackageInfo(code, 'PostNL')
        result.carrier_name = 'PostNL'
        result.status = 'Label created'
        result.status_category = StatusCategory.LABEL_CREATED
        if data['isDelivered']:
            result.status = 'Delivered'
            result.status_category = StatusCategory.DELIVERED
        result.description = data['description']

        for event in data['observations']:
            stamp = datetime.fromisoformat(event['observationDate'])
            e = PackageEvent(stamp, '', event['description'])
            result.events.append(e)

        r = Address()
        r.name = data['recipient']['names']['companyName']
        r.attention = data['recipient']['names']['personName']
        r.address1 = data['recipient']['address']['street'] + ' ' + data['recipient']['address']['houseNumber']
        if data['recipient']['address']['houseNumberSuffix']:
            r.address1 += ' ' + data['recipient']['address']['houseNumberSuffix']
        r.postalcode = data['recipient']['address']['postalCode']
        r.city = data['recipient']['address']['town']
        r.country = data['recipient']['address']['country']
        result.destination = r

        s = Address()
        s.name = data['sender']['names']['companyName']
        s.attention = data['sender']['names']['personName']
        s.address1 = data['sender']['address']['street'] + ' ' + data['sender']['address']['houseNumber']
        if data['sender']['address']['houseNumberSuffix']:
            s.address1 += ' ' + data['sender']['address']['houseNumberSuffix']

        s.postalcode = data['sender']['address']['postalCode']
        s.city = data['sender']['address']['town']
        s.country = data['sender']['address']['country']
        result.origin = s

        try:
            result.weight = int(data['details']['dimensions']['weight'])
        except:
            pass
        return result


class UPS(Carrier):
    DISPLAYNAME = 'UPS'

    @classmethod
    def identify(cls, code):
        if not code.startswith('1Z'):
            return False

        a = 0
        b = 0
        x = True
        for char in code[2:-1]:
            if char.isdigit():
                num = int(char)
            else:
                num = (ord(char) - 63) % 10
            if x:
                a += int(num)
            else:
                b += int(num) * 2
            x = not x
        s = (a + b) % 10
        calculated = 10 - s
        if code[-1].isdigit():
            check = int(code[-1])
        else:
            check = (ord(code[-1]) - 63) % 10

        return calculated == check

    def get_info(self, code, extra):
        session = requests.Session()
        user_url = 'https://www.ups.com/track'
        response = session.get(user_url)
        token = response.cookies['X-XSRF-TOKEN-ST']
        url = 'https://www.ups.com/track/api/Track/GetStatus'
        request_data = {
            'Locale': 'en_US',
            'Requester': 'st/trackdetails',
            'TrackingNumber': [code]
        }
        response = session.post(url, json=request_data, headers={
            'Content-Type': 'Application/Json',
            'Referer': 'https://www.ups.com/track',
            'DNT': '1',
            'User-Agent': 'Mozilla/5.0',
            'X-Requested-With': 'XMLHttpRequest',
            "Accept": "application/json, text/plain, */*",
            'X-XSRF-TOKEN': token
        })
        payload = response.json()
        details = payload['trackDetails'][0]
        result = PackageInfo(code, 'UPS')
        result.carrier_name = 'UPS'
        result.status = details['packageStatus']
        result.status_category = StatusCategory.LABEL_CREATED
        if details['progressBarType'] == 'InTransit':
            result.status_category = StatusCategory.IN_TRANSIT
        elif details['progressBarType'] == 'Delivered':
            result.status_category = StatusCategory.DELIVERED

        for event in details['shipmentProgressActivities']:
            better_time = event['time'].replace('.M.', 'M').zfill(8)
            dt = event['date'] + ' ' + better_time
            pdt = datetime.strptime(dt, '%m/%d/%Y %I:%M %p')
            label = 'No details'
            if event['actCode'] == 'MP':
                label = 'Label created'
            elif event['actCode'] == 'OF':
                label = 'Out for delivery'
            if event['milestoneName'] is not None and 'name' in event['milestoneName']:
                label = event['milestoneName']['name']
            elif 'activityScan' in event and event['activityScan'] is not None:
                label = event['activityScan']
            e = PackageEvent(pdt, event['location'], label)
            result.events.append(e)

        if details['shipToAddress'] is not None:
            a = details['shipToAddress']
            sa = Address()
            sa.name = a['companyName']
            sa.attention = a['attentionName']
            sa.address1 = a['streetAddress1']
            sa.address2 = a['streetAddress2']
            sa.address3 = a['streetAddress3']
            sa.postalcode = a['zipCode']
            sa.city = a['city']
            sa.state = (a['state'] + ' ' + a['province']).strip()
            sa.country = a['country']
            result.destination = sa

        if details['additionalInformation'] is not None:
            ai = details['additionalInformation']
            if 'weight' in ai:
                weight = ai['weight']
                unit = ai['weightUnit']
                if unit == 'KGS':  # WTF ups, that's not how you write it
                    result.weight = int(float(weight) * 1000)
            if 'serviceInformation' in ai and ai['serviceInformation'] is not None:
                result.service = ai['serviceInformation']['serviceName'].replace('&#174;', '')

        if 'leftAt' in details and details['leftAt'] is not None and details['leftAt'] != '':
            result.delivery_note = details['leftAt']
        return result
