import shipments.carrier as carriers


def identify_tracking(code):
    options = []
    cs = get_carriers()
    for c in cs:
        definition = getattr(carriers, c)
        if definition.identify(code):
            extra = definition.get_requirements(code)
            options.append((c, cs[c], extra))
    return options


def get_tracking_info(code, extra=None, carrier=None):
    options = []
    extra = extra if extra is not None else []
    for c in get_carriers():
        definition = getattr(carriers, c)
        if definition.identify(code):
            options.append(definition())

    if len(options) == 0:
        return None
    elif len(options) > 1:
        return False
    instance = options[0]

    return instance.get_info(code, extra)


def get_carrier(carrier_code):
    if not hasattr(carriers, carrier_code):
        return None
    instance = getattr(carriers, carrier_code)()
    return instance


def get_carriers():
    result = {}
    for cn, cls in carriers.__dict__.items():
        if cn.startswith('__'):
            continue
        if cn == 'Carrier':
            continue
        if not hasattr(cls, 'DISPLAYNAME'):
            continue
        result[cn] = cls.DISPLAYNAME
    return result
