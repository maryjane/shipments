Shipments
=========

A python library and a GTK3 mobile/desktop application for tracking multiple shipments.

![Screenshot of the control application](https://brixitcdn.net/metainfo/shipments.png)

Supported carriers
------------------

* UPS
* PostNL

Installing
----------

```shell-session
$ meson build
$ meson compile -C build
$ sudo meson install -C build
```