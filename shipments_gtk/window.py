import json
import os

import gi
import shipments
from shipments.structs import StatusCategory
from shipments_gtk.lookup import get_countries_alpha2

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, GObject, Gio, Gdk, GLib

gi.require_version('Handy', '1')
from gi.repository import Handy


class NewPackageDialog(Gtk.Dialog):
    def __init__(self, parent):
        super().__init__(title="Add package", transient_for=parent, flags=0)
        self.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK
        )

        label = Gtk.Label("Enter the tracking code for the package you wish to track.", xalign=0.0)
        label.set_line_wrap(True)
        label.set_margin_top(16)
        label.set_margin_bottom(4)
        label.set_margin_start(10)
        label.set_margin_end(10)

        self.code = Gtk.Entry()
        self.code.set_margin_start(10)
        self.code.set_margin_end(10)
        self.code.connect('changed', self.on_guess)

        label2 = Gtk.Label("Select the courier", xalign=0.0)
        label2.set_margin_top(16)
        label2.set_margin_bottom(4)
        label2.set_margin_start(10)
        label2.set_margin_end(10)

        dataset = shipments.get_carriers()
        self.carrier = Gtk.ComboBoxText()
        self.carrier.set_entry_text_column(0)
        self.lut = {}
        for i, carrier in enumerate(dataset):
            self.carrier.append_text(dataset[carrier])
            self.lut[carrier] = i
        self.carrier.set_margin_start(10)
        self.carrier.set_margin_end(10)
        self.carrier.set_margin_bottom(16)
        self.carrier.connect('changed', self.on_carrier_change)

        box = self.get_content_area()
        box.add(label)
        box.add(self.code)
        box.add(label2)
        box.add(self.carrier)
        self.extrabox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        box.add(self.extrabox)
        self.show_all()

    def on_guess(self, *args):
        code = self.code.get_text()
        if len(code) > 2:
            guesses = shipments.identify_tracking(code)
            if len(guesses) == 1:
                self.set_carier(guesses[0])
            elif len(guesses) == 0:
                pass
            else:
                self.set_carier(guesses[0])

    def set_carier(self, option):
        self.carrier.set_active(self.lut[option[0]])

    def on_carrier_change(self, *args):
        for child in self.extrabox:
            self.extrabox.remove(child)

        carrier = shipments.get_carrier(self.carrier.get_active_text())

        extra_data = carrier.get_requirements(self.code.get_text())
        for fieldname, fieldlabel, fieldtype in extra_data:
            part = fieldtype.split(':', maxsplit=1)
            default = None
            if len(part) > 1:
                fieldtype = part[0]
                default = part[1]
            control = None
            if fieldtype == 'text':
                control = Gtk.Entry()
            elif fieldtype == 'country-alpha2':
                control = Gtk.ComboBoxText()
                index = None
                for i, cc in enumerate(get_countries_alpha2()):
                    control.append_text(cc)
                    if cc == default:
                        index = i
                if index is not None:
                    control.set_active(index)

            if control is not None:
                label = Gtk.Label(fieldlabel, xalign=0.0)
                label.set_line_wrap(True)
                label.set_margin_top(16)
                label.set_margin_bottom(4)
                label.set_margin_start(10)
                label.set_margin_end(10)

                control.set_margin_start(10)
                control.set_margin_end(10)
                control.fieldname = fieldname

                self.extrabox.add(label)
                self.extrabox.add(control)
        self.extrabox.show_all()


class ShipmentsWindow:
    def __init__(self, application, args, version):
        self.application = application
        self.args = args
        self.version = version

        Handy.init()

        builder = Gtk.Builder()
        builder.add_from_resource('/org/postmarketos/Shipments/ui/shipments.glade')
        builder.connect_signals(self)
        css = Gio.resources_lookup_data("/org/postmarketos/Shipments/ui/style.css", 0)
        self.provider = Gtk.CssProvider()
        self.provider.load_from_data(css.get_data())

        self.provider = Gtk.CssProvider()
        self.provider.load_from_data(css.get_data())

        self.window = builder.get_object("window")
        self.window.set_application(self.application)
        self.mainstack = builder.get_object("mainstack")

        self.packagelist = builder.get_object("packagelist")
        self.package_label = builder.get_object("package_label")
        self.package_code = builder.get_object("package_code")
        self.package_courier = builder.get_object("package_courier")
        self.package_status = builder.get_object("package_status")
        self.package_events = builder.get_object("package_events")
        self.package_delivery_note = builder.get_object("package_delivery_note")
        self.package_description = builder.get_object("package_description")
        self.package_weight = builder.get_object("package_weight")
        self.package_recipient = builder.get_object("package_recipient")
        self.package_sender = builder.get_object("package_sender")
        self.refresh = builder.get_object("refresh")
        self.searchrevealer = builder.get_object("searchrevealer")
        self.searchbox = builder.get_object("searchbox")
        self.leaflet = builder.get_object("leaflet")
        self.topleaflet = builder.get_object("topleaflet")
        self.back = builder.get_object("back")

        self.label_stack = builder.get_object("label_stack")
        self.package_label_entry = builder.get_object("package_label_entry")
        self.package_label_edit = builder.get_object("package_label_edit")

        # self.hamburgermenu = builder.get_object("hamburgermenu")

        self.apply_css(self.window, self.provider)

        self.action_refresh = None
        self.action_delete = None
        self.init_actions()

        xdg_data_home = os.path.expanduser(os.getenv('XDG_DATA_HOME', '~/.local/share'))
        self.db_path = os.path.join(xdg_data_home, 'shipments/shipments.json')
        self.packages = []
        self.current_package = None
        self.db_load()

        if len(self.packages) > 0:
            self.show_package(self.packages[0]['code'])
        else:
            self.db_save()

        self.window.show()

        Gtk.main()

    def apply_css(self, widget, provider):
        Gtk.StyleContext.add_provider(widget.get_style_context(),
                                      provider,
                                      Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        if isinstance(widget, Gtk.Container):
            widget.forall(self.apply_css, provider)

    def init_actions(self):
        action = Gio.SimpleAction.new("about", None)
        action.connect("activate", self.on_about)
        self.application.add_action(action)

        action = Gio.SimpleAction.new("add", None)
        action.connect("activate", self.on_add)
        self.application.add_action(action)

        action = Gio.SimpleAction.new("refresh", None)
        action.connect("activate", self.on_refresh)
        self.application.add_action(action)
        self.action_refresh = action

        action = Gio.SimpleAction.new("search", None)
        action.connect("activate", self.on_search)
        self.application.add_action(action)

        action = Gio.SimpleAction.new("delete", None)
        action.connect("activate", self.on_delete)
        self.application.add_action(action)
        self.action_delete = action

    def db_load(self):
        if not os.path.isfile(self.db_path):
            return
        with open(self.db_path) as handle:
            self.packages = json.loads(handle.read())
        self.rebuild_list()

    def db_save(self):
        if not os.path.isdir(os.path.dirname(self.db_path)):
            os.makedirs(os.path.dirname(self.db_path))

        encoded = json.dumps(self.packages)
        with open(self.db_path, 'w') as handle:
            handle.write(encoded)

    def on_main_window_destroy(self, widget):
        Gtk.main_quit()

    def rebuild_list(self):
        for child in self.packagelist:
            self.packagelist.remove(child)

        self.action_delete.set_enabled(len(self.packages) > 0)
        self.action_refresh.set_enabled(len(self.packages) > 0)

        for package in self.packages:
            box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
            label = Gtk.Label(label=package['label'], xalign=0.0)
            box.add(label)
            code = Gtk.Label(label=package['status'], xalign=0.0)
            code.get_style_context().add_class('dim-label')
            box.add(code)
            box.set_margin_top(8)
            box.set_margin_bottom(8)
            box.set_margin_start(10)
            box.set_margin_end(10)
            row = Gtk.ListBoxRow()
            row.add(box)
            row.label = package['label']
            row.code = package['code']
            self.packagelist.add(row)
        self.packagelist.show_all()

    def show_package(self, code):
        self.mainstack.set_visible_child_name('package')
        self.current_package = code

        for package in self.packages:
            if package['code'] == code:
                break
        else:
            raise ValueError('Package not found')

        self.package_label.set_text(package['label'])
        self.package_code.set_text(package['code'])
        self.package_status.set_text(package['status'])
        self.package_courier.set_text(package['courier_display'])
        self.package_delivery_note.set_text(package['note'] if package['note'] is not None else 'n/a')
        self.package_description.set_text(package['description'] if package['description'] is not None else '')
        if package['weight']:
            self.package_weight.set_text(str(package['weight'] / 1000) + ' Kg')
        else:
            self.package_weight.set_text('')
        self.package_recipient.set_text(package['recipient'] if package['recipient'] is not None else 'n/a')
        self.package_sender.set_text(package['sender'] if package['sender'] is not None else 'n/a')

        for child in self.package_events:
            self.package_events.remove(child)

        for event in package['events']:
            status = Gtk.Label(event['label'], xalign=0.0)
            location = Gtk.Label(event['location'], xalign=0.0)
            location.get_style_context().add_class('dim-label')
            timestamp = Gtk.Label(event['timestamp'].replace('T', ' ')[:-3], xalign=0.0)
            timestamp.get_style_context().add_class('dim-label')

            status.set_margin_top(8)
            status.set_margin_bottom(4)
            status.set_margin_start(8)
            status.set_margin_end(8)
            status.set_line_wrap(True)

            location.set_margin_bottom(8)
            location.set_margin_start(8)
            location.set_margin_end(8)

            timestamp.set_margin_bottom(8)
            timestamp.set_margin_start(8)
            timestamp.set_margin_end(8)

            sep = Gtk.Separator()
            box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
            box.add(status)
            box2 = Gtk.Box()
            box.add(box2)
            box.add(sep)
            box2.add(location)
            box2.pack_end(timestamp, 0, 0, 0)
            self.package_events.add(box)
        self.package_events.show_all()

    def refresh_package(self, code):
        for i, package in enumerate(self.packages):
            if package['code'] == code:
                break
        else:
            raise ValueError('Package not found')

        print("Refreshing index {}".format(i))
        extra = self.packages[i]['extra']
        info = shipments.get_tracking_info(code, extra, package['courier'])
        self.packages[i]['status'] = info.status
        self.packages[i]['courier_display'] = info.carrier_name
        self.packages[i]['events'] = []
        self.packages[i]['status_category'] = info.status_category.value
        self.packages[i]['note'] = info.delivery_note
        self.packages[i]['description'] = info.description
        self.packages[i]['weight'] = info.weight

        for event in info.events:
            self.packages[i]['events'].append({
                'timestamp': event.timestamp.isoformat(),
                'location': event.location,
                'label': event.label
            })

        if info.destination is not None:
            self.packages[i]['recipient'] = str(info.destination)
        else:
            self.packages[i]['recipient'] = None
        if info.origin is not None:
            self.packages[i]['sender'] = str(info.origin)
        else:
            self.packages[i]['sender'] = None

        self.db_save()
        self.rebuild_list()

    def on_about(self, *args):
        dialog = Gtk.AboutDialog(transient_for=self.window)
        dialog.set_logo_icon_name('org.postmarketos.Shipments')
        dialog.set_program_name('Shipments')
        dialog.set_version(self.version)
        dialog.set_website('https://git.sr.ht/~martijnbraam/shipments')
        dialog.set_authors(['Martijn Braam'])
        gtk_version = '{}.{}.{}'.format(Gtk.get_major_version(),
                                        Gtk.get_minor_version(), Gtk.get_micro_version())
        comment = "Postal package tracking application\n\n"
        comment += 'Gtk: {}'.format(gtk_version)
        dialog.set_comments(comment)
        text = "Distributed under the GNU GPL(v3) license.\n"
        dialog.set_license(text)
        dialog.run()
        dialog.destroy()

    def on_refresh(self, action, *args):
        action.set_enabled(False)
        self.refresh_package(self.current_package)
        action.set_enabled(True)
        self.show_package(self.current_package)

    def on_add(self, action, *args):
        dialog = NewPackageDialog(self.window)
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            code = dialog.code.get_text()
            carrier_name = dialog.carrier.get_active_text()

            carrierlist = shipments.get_carriers()
            for carrier_code in carrierlist:
                if carrierlist[carrier_code] == carrier_name:
                    break
            else:
                raise ValueError("Carrier code unknown")

            extra = {}
            for control in dialog.extrabox:
                if hasattr(control, 'fieldname'):
                    if isinstance(control, Gtk.Entry):
                        extra[control.fieldname] = control.get_text()
                    elif isinstance(control, Gtk.ComboBoxText):
                        extra[control.fieldname] = control.get_active_text()
                    else:
                        print(control)

            print(f"Adding tracking {code} from {carrier_code}")
            print("Extra data: ", extra)
            for package in self.packages:
                if package['code'] == code:
                    print("Code already exists in database, ignoring")
                    return

            self.packages.append({
                'label': code,
                'code': code,
                'courier': carrier_code,
                'extra': extra,
                'courier_display': carrier_name,
                'status': 'unknown',
                'status_category': StatusCategory.LABEL_CREATED.value,
                'note': '',
                'description': '',
                'weight': '',
                'recipient': '',
                'sender': '',
                'events': []
            })
            self.db_save()
            self.db_load()

        dialog.destroy()

    def on_search(self, action, *args):
        self.searchrevealer.set_reveal_child(True)
        self.searchbox.grab_focus()

    def on_row_activated(self, widget, row):
        if self.packagelist.get_selection_mode() == Gtk.SelectionMode.NONE:
            self.packagelist.set_selection_mode(Gtk.SelectionMode.SINGLE)
            self.packagelist.select_row(row)
        self.show_package(row.code)
        self.leaflet.set_visible_child_name('content')

        if self.leaflet.get_folded():
            self.packagelist.unselect_row(row)

    def on_leaflet_change(self, *args):
        self.topleaflet.set_visible_child_name(self.leaflet.get_visible_child_name())
        self.back.set_visible(self.leaflet.get_folded())

    def on_back_clicked(self, *args):
        self.leaflet.set_visible_child_name('sidebar')

    def on_label_edit_toggle(self, widget, *args):
        view = 'entry' if widget.get_active() else 'label'
        if widget.get_active():
            self.package_label_entry.set_text(self.package_label.get_text())
            self.package_label_entry.grab_focus()
        self.label_stack.set_visible_child_name(view)

    def on_label_activate(self, widget, *args):
        new_label = widget.get_text()
        for i, package in enumerate(self.packages):
            if package['code'] == self.current_package:
                break
        else:
            raise ValueError('Package not found')

        self.packages[i]['label'] = new_label

        self.db_save()
        self.rebuild_list()
        self.package_label.set_text(new_label)
        self.package_label_edit.set_active(False)

    def on_delete(self, *args):
        for i, package in enumerate(self.packages):
            if package['code'] == self.current_package:
                break
        else:
            raise ValueError('Package not found')

        del self.packages[i]

        self.db_save()
        self.rebuild_list()

        if len(self.packages) > 0:
            self.show_package(self.packages[0]['code'])
        else:
            self.mainstack.set_visible_child_name('nopackages')
