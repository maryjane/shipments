import argparse
import os
import gi

from shipments_gtk.window import ShipmentsWindow

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio

gi.require_version('Handy', '1')
from gi.repository import Handy


class ShipmentsApplication(Gtk.Application):
    def __init__(self, application_id, flags, args, version):
        Gtk.Application.__init__(self, application_id=application_id, flags=flags)
        self.connect("activate", self.new_window)
        self.args = args
        self.version = version

    def new_window(self, *args):
        ShipmentsWindow(self, self.args, self.version)


def main(version):
    Handy.init()

    parser = argparse.ArgumentParser(description="Shipment tracker")
    args = parser.parse_args()

    if os.path.isfile('shipments_gtk/shipments.gresource'):
        print("Using resources from cwd/shipments_gtk")
        resource = Gio.resource_load("shipments.gresource")
        Gio.Resource._register(resource)
    elif os.path.isfile('shipments.gresource'):
        print("Using resources from cwd")
        resource = Gio.resource_load("shipments.gresource")
        Gio.Resource._register(resource)

    app = ShipmentsApplication("org.postmarketos.Shipments", Gio.ApplicationFlags.FLAGS_NONE, args=args,
                               version=version)
    app.run()


if __name__ == '__main__':
    main('development')
